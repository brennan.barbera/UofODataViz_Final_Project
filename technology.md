# Technologies Used
## Data Cleaning and Analysis
Pandas will be used to clean the data and perform an exploratory analysis. Further analysis will be completed using Python within Google Colab.

## Database Storage
Postgres(cloud) is the database we intend to use. 

## Machine Learning
A random forest classifier is what we are intending to use.

## Dashboard
We intend to use Tableau to create a presentable dashboard.
